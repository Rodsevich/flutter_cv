import 'package:flutter/material.dart';
import 'package:flutter_cv/src/pages/home/ui.dart';

void main() {
  runApp(MaterialApp(
    title: 'CV Nicolas Rodsevich',
    theme: ThemeData(
      primarySwatch: Colors.blueGrey,
      // textTheme: TextTheme(
      //     bodyText1: TextStyle(fontFamily: 'FrederickaTheGreat'),
      //     bodyText2: TextStyle(fontFamily: 'FrederickaTheGreat')),
      // This makes the visual density adapt to the platform that you run
      // the app on. For desktop platforms, the controls will be smaller and
      // closer together (more dense) than on mobile platforms.
      visualDensity: VisualDensity.adaptivePlatformDensity,
      backgroundColor: Colors.white,
    ),
    routes: {
      '/': (_) => HomePage(),
    },
  ));
}
