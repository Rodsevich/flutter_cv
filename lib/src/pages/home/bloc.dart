// TODO Implement this library.

import 'package:bloc/bloc.dart';
import 'package:flutter_cv/src/pages/home/estados.dart';
import 'package:flutter_cv/src/pages/home/eventos.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc(HomeState initialState) : super(initialState);

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    StatusQuoHome estado = state;
    var estilo = estado.estilo;
    var faceta = estado.faceta;
    var grado = estado.gradoFormalidad;
    var nombre = estado.nombre;
    if (event is FormalidadCambio) {
      grado = event.grado;
      switch (grado.toInt()) {
        case 0:
          nombre = "Nicolás Sergio Rodsevich";
          break;
        case 1:
          nombre = "Nicolás S. Rodsevich";
          break;
        case 2:
          nombre = "Nicolás Rodsevich";
          break;
        case 3:
          nombre = "Nico Rodsevich";
          break;
        case 4:
          nombre = "Nico";
          break;
      }
    } else if (event is FacetaCambio) {
      faceta = event.faceta;
    }
    yield StatusQuoHome(
        estilo: estilo, faceta: faceta, nombre: nombre, gradoFormalidad: grado);
  }
}
