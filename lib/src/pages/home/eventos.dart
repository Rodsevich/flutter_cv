import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FormalidadCambio extends HomeEvent {
  final double grado;

  FormalidadCambio(this.grado);

  @override
  List<Object> get props => [grado];

  @override
  String toString() => "Cambio de formalidad a nivel $grado";
}

enum Faceta { profesor, desarrollador }

class FacetaCambio extends HomeEvent {
  final Faceta faceta;

  FacetaCambio(this.faceta);

  @override
  List<Object> get props => [faceta];

  @override
  String toString() => "Cambio de faceta a $faceta";
}
