import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'eventos.dart';

abstract class HomeState extends Equatable {}

class StatusQuoHome extends HomeState {
  final String nombre;
  final Faceta faceta;
  final double gradoFormalidad;
  final String estilo;

  StatusQuoHome({this.estilo, this.nombre, this.faceta, this.gradoFormalidad});

  String get pathImagen => 'images/formal${gradoFormalidad.toInt()}.jpg';

  @override
  List<Object> get props => [nombre, faceta, gradoFormalidad];
}
