import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_cv/src/pages/home/bloc.dart';
import 'package:flutter_cv/src/pages/home/estados.dart';
import 'package:flutter_cv/src/pages/home/eventos.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  final double cardWidth = 850.0, cardHeight = 590.0;
  @override
  _HomePageState createState() => _HomePageState();
}

class _DirectivasFont {
  final String fontFamily;
  final double tamanio;

  _DirectivasFont(this.fontFamily, this.tamanio);
}

class _HomePageState extends State<HomePage> {
  Faceta faceta = Faceta.desarrollador;
  var estiloFacetaActiva = TextStyle(
      fontSize: 16, color: Colors.blueGrey, fontWeight: FontWeight.w900);
  var estiloFacetaInactiva = TextStyle(
      fontSize: 14, color: Colors.black87, fontWeight: FontWeight.w300);
  List<_DirectivasFont> fonts = [
    _DirectivasFont('IbmPlexSans', 62),
    _DirectivasFont('Merriweather', 66),
    _DirectivasFont('Sahitya', 66),
    _DirectivasFont('Bangers', 60),
    _DirectivasFont('IndieFlower', 146),
    _DirectivasFont('FrederickaTheGreat', 64),
  ];

  Widget _textoNombre(
      StatusQuoHome state, BoxConstraints constraints, _DirectivasFont dirs) {
    return Text(
      state.nombre,
      softWrap: true,
      maxLines: 3,
      textAlign: TextAlign.center,
      style: TextStyle(
          fontFamily: dirs.fontFamily,
          fontSize:
              constraints.maxWidth < 630 ? dirs.tamanio * 0.6 : dirs.tamanio),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
        create: (context) => HomeBloc(StatusQuoHome(
            faceta: Faceta.desarrollador,
            nombre: "Nicolás Rodsevich",
            gradoFormalidad: 2.0,
            estilo: 'Merriweather')),
        child: Scaffold(
            backgroundColor: Colors.black12,
            body: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                return Center(
                  heightFactor: 1.3,
                  child: SingleChildScrollView(
                    child: Container(
                      child: Card(
                        elevation: 6,
                        child: Container(
                          width: max(300,
                              constraints.constrainWidth(widget.cardWidth)),
                          height: widget.cardHeight,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: [
                                Color.fromRGBO(230, 230, 230, 1),
                                Color.fromRGBO(255, 250, 250, 1)
                              ])),
                          padding: EdgeInsets.only(top: 12.0),
                          child: Column(
                            children: [
                              _encabezadoDinamico(constraints),
                              Divider(
                                color: Colors.black,
                                height: 10,
                                thickness: 0.4,
                              ),
                              _contenidos(constraints, faceta)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              },
            )));
  }

  Widget _encabezadoDinamico(BoxConstraints constraints) {
    return BlocBuilder<HomeBloc, HomeState>(
        builder: (BuildContext context, state) {
      if (state is StatusQuoHome) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.asset(
                  state.pathImagen,
                  width: min(200, constraints.maxWidth * 0.24),
                  height: 250,
                  fit: BoxFit.fitHeight,
                )),
            Column(
              children: [
                ConstrainedBox(
                    constraints: BoxConstraints(
                        maxWidth: min(widget.cardWidth * 0.74,
                            constraints.maxWidth * 0.73),
                        minHeight: constraints.maxWidth < 450 ? 100 : 150,
                        maxHeight: constraints.maxWidth < 450 ? 150 : 200),
                    child: _textoNombre(
                        state,
                        constraints,
                        state.faceta == Faceta.profesor
                            ? fonts.last
                            : fonts[state.gradoFormalidad.toInt()])),
                ConstrainedBox(
                  constraints: BoxConstraints(
                      maxWidth: min(widget.cardWidth * 0.74,
                          constraints.maxWidth * 0.74)),
                  child: Wrap(
                    alignment: WrapAlignment.center,
                    children: [
                      // Text('${constraints.maxWidth} x ${constraints.maxHeight}'),
                      FlatButton(
                        onPressed: () {
                          setState(() {
                            faceta = Faceta.desarrollador;
                          });
                          BlocProvider.of<HomeBloc>(context)
                              .add(FacetaCambio(faceta));
                        },
                        child: Text(
                          'Fullstack Dart Developer',
                          style: faceta == Faceta.desarrollador
                              ? estiloFacetaActiva
                              : estiloFacetaInactiva,
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          setState(() {
                            faceta = Faceta.profesor;
                          });
                          BlocProvider.of<HomeBloc>(context)
                              .add(FacetaCambio(faceta));
                        },
                        child: Text(
                          'Prof. Filosofía & Teología',
                          style: faceta == Faceta.profesor
                              ? estiloFacetaActiva
                              : estiloFacetaInactiva,
                        ),
                      ),
                    ],
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(
                      maxWidth: min(widget.cardWidth * 0.73,
                          constraints.maxWidth * 0.73)),
                  child: Row(
                    children: [
                      Text(constraints.maxWidth < 450
                          ? 'Formalidad'
                          : 'Grado de formalidad:'),
                      Expanded(
                        child: Slider(
                          min: 0,
                          max: 4,
                          divisions: 4,
                          value: state.gradoFormalidad,
                          onChanged: (double value) {
                            BlocProvider.of<HomeBloc>(context)
                                .add(FormalidadCambio(value));
                          },
                          label:
                              '"Estos son mis ppios. Si no le gustan tengo otros" - Groucho Marx',
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ],
        );
      } else {
        throw UnimplementedError('q es este estado??');
      }
    });
  }
}

_contenidos(BoxConstraints constraints, Faceta faceta) {
  return ConstrainedBox(
    constraints: BoxConstraints(maxHeight: 300),
    child: Wrap(
      children: faceta == Faceta.desarrollador
          ? [
              _icono(
                  'https://www.linkedin.com/in/nicolas-rodsevich/',
                  'https://logos-marcas.com/wp-content/uploads/2020/04/Linkedin-Logo.png',
                  constraints),
              SizedBox(width: 24),
              _icono(
                  'https://gitlab.com/Rodsevich',
                  'https://blog.softtek.com/hubfs/blogs/innovationlabs/gitlab-logo2.png',
                  constraints)
            ]
          : [
              _icono(
                  'https://www.youtube.com/channel/UCFp0f_R68noiwptH5vo99Ew/videos',
                  'https://neilpatel.com/wp-content/uploads/2015/09/youtube.png',
                  constraints),
            ],
    ),
  );
}

Widget _icono(String link, String urlLogo, BoxConstraints constraints) {
  return InkWell(
    onTap: () {
      launch(link);
    },
    child: SizedBox(
        width: max((min(constraints.maxWidth, 850) - 50) / 2, 200),
        height: constraints.maxWidth > 435 ? 200 : 100,
        child: Image.network(urlLogo)),
  );
}
